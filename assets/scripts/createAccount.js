// modifications choix formation et accompagnateurs 

                    
function test(){
    let valRoleId = document.querySelector('#Role_id').value;
    // affiche la liste des formations quand on sélectionne un stagiaire en formation
    if(valRoleId == '2'){
        
        $("#Training_id").show();
    }
    else {
        
        $("#Training_id").hide();
    }
    // affiche la liste des accompagnateurs quand on sélectionne un stagiaire en accompagnement
    if(valRoleId == '3'){
        
        $("#Coach_Salaried_Account_id").show();
    }
    else {
        
        $("#Coach_Salaried_Account_id").hide();
    }
    // affiche la liste des coachs quand on sélectionne une entreprise
    if(valRoleId == '7'){
        
        $("#Salaried_Account_id").show();
    }
    else {
        
        $("#Salaried_Account_id").hide();
    }
}
$().ready(function () {
    
    // on cache les div qui ne doivent pas etre utilisées
    $("#Training_id").hide();
    $("#Coach_Salaried_Account_id").hide();
    $("#Salaried_Account_id").hide();
       
    // fin modifications choix formation et accompagnateurs 

    //* quand la page est chargée et au click sur le bouton connect
    $('#button_create_account').click(function () {
        //* recupère les données du formulaire dans une variable
        var form_data = $('#create_account_form').serialize();
        //console.log(form_data);


        //* requête ajax en POST vers la page qui gère la connexion + data_form
        $.ajax({
            method: "POST",
            url: "/account/",
            data: form_data,
            //* si la requête réussi 
            success: function (result) {
                //alert(result);
                if (result === 'true') {
                    $('#response').html(
                        "<div class='alert alert-success'>Compte créé avec succès !  </div>")
                    $('#button_create_account').html(
                        "Enregistrement <img width='20' height='20' src='assets/images/loadeer.gif' alt='loader'>")


                    setTimeout(
                        function () {
                            window.location.href = "/home";
                        }, 2000);

                } else {
                    $('#response').html(
                        "<div class='alert alert-danger'>Il y a un probleme !!!</div>")
                }
            },

            error: function () {
                window.location.replace("/404");
            }

        })
    })

});