<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\models\Skill;
use BWB\Framework\mvc\models\Advertisment;
use BWB\Framework\mvc\models\Company;
use BWB\Framework\mvc\dao\DAOExpertise;

class Expertise extends DefaultModel
{
    protected $Advertisment_id;
    protected $Advertisment_Company_id;
    protected $Skill_id;

    public function __construct($id = null)
    {
        if(!is_null($id)){

            $this->parse((new DAOExpertise())->retrieve($id));
        }
    }

    public function setAdvertisment_id($Advertisment_id)
    {
        $this->Advertisment_id = new Advertisment($Advertisment_id);
    }

    public function getAdvertisment_id()
    {
        return $this->Advertisment_id;
    }
//! #########################  voir advertissement ??
    public function setAdvertisment_Company_id($Advertisment_Company_id)
    {
        $this->Advertisment_Company_id = new Company($Advertisment_Company_id);
    }

    public function getAdvertisment_Company_id()
    {
        return $this->Advertisment_Company_id;
    }

    public function setSkill_id($Skill_id)
    {
        $this->Skill_id = new Skill($Skill_id);
    }

    public function getSkill_id()
    {
        return $this->Skill_id;
    }
}