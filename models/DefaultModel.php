<?php

namespace BWB\Framework\mvc\models;



class DefaultModel
{



    public function parse($array)
    {
        // On va parcourir le tableau récupèré pour hydrater
        foreach ($array as $key => $value) {
            // On récupère le nom du setter correspondant à $key
            $method = 'set' . ucfirst($key); // ucfirst()=> 1ere lettre en majuscule
            // On vérifie si le setter existe
            if (method_exists($this, $method)) {
                // On appelle le setter
                $this->$method($value);
            }
        }
    }
}

