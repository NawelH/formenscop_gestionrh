<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\dao\DAOCoach;
use BWB\Framework\mvc\models\Salaried;

class Coach extends DefaultModel
{
    //* Propriétés
    protected $Salaried_Account_id;


    //* Constructeur
    public function __construct($id)
    {
        if (!is_null($id)) {

            $this->parse((new DAOCoach)->retrieve($id));
        }
    }


    //* Getter
    function getSalaried_Account_id()
    {
        return $this->Salaried_Account_id;
    }


    //* Setter
    public function setSalaried_Account_id($Account_id)
    {
        $this->Salaried_Account_id = new Salaried($Account_id);
    }
}
