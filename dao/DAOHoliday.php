<?php

namespace BWB\Framework\mvc\dao;

use BWB\Framework\mvc\DAO;
use PDO;

class DAOHoliday extends DAO
{

    /**
     * Requête qui récupère uniquement les demandes de congés 
     *
     * @return array
     */
    public function getHolidayRequests()
    {
        $response = $this->getPdo()->query('SELECT * FROM Holiday WHERE status = "demande" ');
        $response->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Holiday');
        return $response->fetchAll();
    }

   
    /**
     * Requête qui récupère uniquement les congés validés
     *
     * @return array
     */
    public function getHolidays()
    {
        $response = $this->getPdo()->query('SELECT * FROM Holiday WHERE status = "valid" ');
        $response->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Holiday');
        return $response->fetchAll();
    }

    /**
     * Requête qui valide une demande de congés
     *
     * @param  mixed $array
     *
     * @return boolean
     */
    public function validateRequest($array)
    {
        $id = $array["edit"];
        $query = "UPDATE Holiday SET `status`='valid' WHERE id=" . $id;
        $req = $this->getPdo()->prepare($query);
        $req->execute();
    }

    /**
     * Requête qui refuse une demande de congés
     *
     * @param  mixed $array
     *
     * @return boolean
     */
    public function refuseRequest($array)
    {
        $id = $array["edit"];
        $query = "UPDATE Holiday SET `status`='refused' WHERE id=" . $id;
        $req = $this->getPdo()->prepare($query);
        $req->execute();
    }


    public function create($array)
    { }

    public function delete($id)
    { }

    public function getAll()
    { }

    public function getAllBy($filter)
    { }

    public function retrieve($id)
    { }

    public function update($array)
    { }
}
