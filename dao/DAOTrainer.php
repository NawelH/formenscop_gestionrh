<?php

namespace BWB\Framework\mvc\dao;

use BWB\Framework\mvc\DAO;
use PDO;

class DAOTrainer extends DAO
{

    public function create($array)
    { }

    public function delete($id)
    { }

    public function getAll()
    {
        $result = $this->getPdo()->query('SELECT * FROM Trainer');
        $result->setFetchMode(PDO::FETCH_CLASS, 'BWB\Framework\mvc\models\Trainer');
        return $result->fetchAll();
    }

    public function getAllBy($filter)
    { }

    public function retrieve($id)
    {
     
    }

    public function update($array)
    { }
}