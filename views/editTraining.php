<!DOCTYPE html>
<html>

<head>
	<?php include('include/head.php'); ?>
</head>

<body>
	<?php include('include/header.php'); ?>
	<?php include('include/sidebar.php'); ?>
	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4>Edit training</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="/home">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Edit training</li>
								</ol>
							</nav>
						</div>
						<div class="col-md-6 col-sm-12 text-right">
							<div class="dropdown">
								<a class="btn btn-secondary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
									Juillet 2019
								</a>
								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="#">Export List</a>
									<a class="dropdown-item" href="#">Policies</a>
									<a class="dropdown-item" href="#">View Assets</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Default Basic Forms Start -->
				<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
					<!-- FORMULAIRE -->
					<!-- FORMULAIRE -->
					<!-- FORMULAIRE -->
					<form id="update_training_form">

						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Name </label>
							<div class="col-sm-12 col-md-10">
								<input class="form-control" type="text" placeholder="name formation" name="name" value="<?= $datas[0]->getName(); ?>">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Start</label>
							<div class="form-group col-md-10">
								<input class="form-control" placeholder="2019-09-01" type="text" name="start" value="<?= $datas[0]->getStart(); ?>">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">End</label>
							<div class="form-group col-md-10">
								<input class="form-control" placeholder="2019-09-01" type="text" name="end" value="<?= $datas[0]->getEnd(); ?>">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Nombre de jour</label>
							<div class="col-sm-12 col-md-10">
								<input class="form-control" placeholder="volume formation" type="days" name="volume" value="<?= $datas[0]->getVolume(); ?>">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Formateur</label>
							<div class="col-sm-12 col-md-4">
								<select class="form-control" name="Salaried_Account_id">
									<!-- Poser la valeur initiale -->
									<option value="<?= $datas[0]->idformateur ?>"><?= $datas[0]->formateur ?></option>
									<?php foreach ($datas[1] as $data) : ?>
										<option value="<?= $data->getId(); ?>"><?= $data->getName() . " " . $data->getFirstName() ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label">Coordinateur</label>
							<div class="col-sm-12 col-md-4">
								<select class="form-control" name="Coordinator_Salaried_Account_id">
									<!-- Poser la valeur initiale -->
									<option value="<?= $datas[0]->idcoordinateur ?>"><?= $datas[0]->coordinateur ?></option>
									<?php foreach ($datas[2] as $data) : ?>
										<option value="<?= $data->getId(); ?>"><?= $data->getName() . " " . $data->getFirstName() ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<br>

						<div id="response"></div>

						<br>

						<input id="training_id" type="hidden" name="id" value="<?= $datas[0]->getId(); ?>">
						<button id="button_update_training" type="button" class="btn btn-primary">Editer</button>

					</form>
				</div>
				<!-- Default Basic Forms End -->
			</div>



			<?php include('include/footer.php'); ?>
		</div>
	</div>
	<?php include('include/script.php'); ?>
</body>

</html>